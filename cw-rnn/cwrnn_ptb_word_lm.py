import time
import sys
import numpy as np
import tensorflow as tf
import reader
from tensorflow.python.ops import math_ops
from tensorflow.python.ops.rnn_cell_impl import RNNCell
from tensorflow.python.util import nest
from tensorflow.python.ops import variable_scope as vs
from tensorflow.python.ops import array_ops
from tensorflow.python.ops import init_ops
from tensorflow.python.ops import nn_ops
_BIAS_VARIABLE_NAME = "bias"
_WEIGHTS_VARIABLE_NAME = "kernel"

flags = tf.flags
logging = tf.logging

flags.DEFINE_string(
        "model", "small",
        "A type of model. Possible options are: small, medium, large.")
flags.DEFINE_string("data_path", "../data",
                                        "Where the training/test data is stored.")
flags.DEFINE_string("save_path", None,
                                        "Model output directory.")
flags.DEFINE_bool("use_fp16", False,
                                    "Train using 16-bit floats instead of 32bit floats")

FLAGS = flags.FLAGS


def data_type():
        return tf.float16 if FLAGS.use_fp16 else tf.float32


class CWRNNCell(RNNCell):

    def __init__(self, num_units, periods, activation=None, reuse=None):
        super(CWRNNCell, self).__init__(_reuse=reuse)
        self._num_units = num_units
        self._activation = activation or math_ops.tanh

        self.periods = periods
        self.num_periods = len(self.periods)
        self.block_size = self._num_units / self.num_periods

        clockwork_mask = np.zeros((self._num_units, self._num_units))
        for i in range(self.num_periods):
                clockwork_mask[i * self.block_size:(i+1) * self.block_size, i * self.block_size:] = 1.0
        self.clockwork_mask = tf.constant(clockwork_mask, name = 'clockwork_mask', dtype = tf.float32)



    @property
    def state_size(self):
        return self._num_units

    @property
    def output_size(self):
        return self._num_units

    def step(self, inputs, state, time_step):
        """Most basic RNN: output = new_state = act(W * input + U * state + B)."""
        output = self._activation(self._linear([inputs, state], self._num_units, True, time_step))
        return output, output

    def _linear(self, args,
                    output_size,
                    bias,
                    time_step,
                    bias_initializer=None,
                    kernel_initializer=None):
    
        num_periods = len(self.periods)
        block_size = output_size / num_periods
            
    
        clockwork_mask = np.zeros((output_size, output_size))
        for i in range(num_periods):
                clockwork_mask[i * block_size:(i+1) * block_size, i * block_size:] = 1.0
        clockwork_mask = tf.constant(clockwork_mask, name = 'clockwork_mask', dtype = tf.float32)
    
        for i in range(num_periods):
                if time_step % self.periods[::-1][i] == 0:
                    active_rows = block_size * (num_periods-i)
                    break
        if args is None or (nest.is_sequence(args) and not args):
            raise ValueError("`args` must be specified")
        if not nest.is_sequence(args):
            args = [args]
    
        #Calculate the total size of arguments on dimension 1.
        total_arg_size = 0
        shapes = [a.get_shape() for a in args]
        for shape in shapes:
            if shape.ndims != 2:
                raise ValueError("linear is expecting 2D arguments: %s" % shapes)
            if shape[1].value is None:
                raise ValueError("linear expects shape[1] to be provided for shape %s, "
                                                 "but saw %s" % (shape, shape[1]))
            else:
                total_arg_size += shape[1].value
    
        dtype = [a.dtype for a in args][0]
        
        # Now the computation.
        scope = vs.get_variable_scope()
        with vs.variable_scope(scope) as outer_scope:
            weights = vs.get_variable(
                    _WEIGHTS_VARIABLE_NAME, [total_arg_size, output_size],
                    dtype=dtype,
                    initializer=tf.contrib.layers.xavier_initializer(seed=0))
    
    
            weights_inputs = tf.slice(weights, [0,0], [shapes[0][1].value, output_size])
            weights_state = tf.slice(weights, [shapes[0][1].value,0], [shapes[1][1].value, output_size])
    
            #cwrnn weights implementation
            weights_state_masked = tf.multiply(weights_state, clockwork_mask)
            weights_state_cw = tf.slice(weights_state_masked, [0, 0], [-1, active_rows])
            weights_inputs_cw = tf.slice(weights_inputs, [0, 0], [-1, active_rows])
    
    
            #print weights_state_masked.get_shape(), weights_state_cw.get_shape(), weights_inputs_cw.get_shape()
            res = math_ops.matmul(array_ops.concat(args, 1), array_ops.concat([weights_inputs_cw, weights_state_cw], 0))
            if not bias:
                return res
            with vs.variable_scope(outer_scope) as inner_scope:
                inner_scope.set_partitioner(None)
                biases = vs.get_variable(
                        _BIAS_VARIABLE_NAME, [output_size],
                        dtype=dtype,
                        initializer=tf.contrib.layers.xavier_initializer(seed=0))
    
                #cwrnn bias implementation
                biases = tf.slice(biases, [0], [active_rows])

            res = nn_ops.bias_add(res, biases)
            
            res = tf.concat(axis=1, values=[res, tf.slice(args[1], [0, active_rows], [-1, -1])])
            return res

class PTBInput(object):

        def __init__(self, config, data, name=None):
                self.batch_size = batch_size = config.batch_size
                self.num_steps = num_steps = config.num_steps
                self.epoch_size = ((len(data) // batch_size) - 1) // num_steps
                self.input_data, self.targets = reader.ptb_producer(data, batch_size, num_steps, name=name)


class PTBModel(object):

        def __init__(self, is_training, config, input_):
                self._input = input_
        
                batch_size = input_.batch_size
                num_steps = input_.num_steps
                size = config.hidden_size
                vocab_size = config.vocab_size
                
                assert size % len(config.periods) == 0
                # Slightly better results can be obtained with forget gate biases
                # initialized to 1 but the hyperparameters of the model would need to be
                # different than reported in the paper.
                rnn_cell = CWRNNCell(size, config.periods)
                #if is_training and config.keep_prob < 1:
                #     lstm_cell = tf.nn.rnn_cell.DropoutWrapper(lstm_cell, output_keep_prob=config.keep_prob)
                #cell = tf.nn.rnn_cell.MultiRNNCell([lstm_cell] * config.num_layers, state_is_tuple=True)
        
                self._initial_state = rnn_cell.zero_state(batch_size, data_type())
        
                with tf.device("/cpu:0"):
                        embedding = tf.get_variable("embedding", [vocab_size, size], dtype=data_type(), initializer=tf.contrib.layers.xavier_initializer(seed=0))
                        inputs = tf.nn.embedding_lookup(embedding, input_.input_data)
        
                if is_training and config.keep_prob < 1:
                        inputs = tf.nn.dropout(inputs, config.keep_prob, seed=0)
        
                
                outputs = []
                state = self._initial_state
                with tf.variable_scope("RNN"):
                        for time_step in range(num_steps):
                                if time_step > 0: 
                                        tf.get_variable_scope().reuse_variables()
                                (cell_output, state) = rnn_cell.step(inputs[:, time_step, :], state, time_step)
                                outputs.append(cell_output)
        
                output = tf.reshape(tf.concat(outputs, 1), [-1, size])
                softmax_w = tf.get_variable(
                        "softmax_w", [size, vocab_size], dtype=data_type(), initializer=tf.contrib.layers.xavier_initializer(seed=0))
                softmax_b = tf.get_variable("softmax_b", [vocab_size], dtype=data_type(), initializer=tf.contrib.layers.xavier_initializer(seed=0))
                logits = tf.matmul(output, softmax_w) + softmax_b
                loss = tf.contrib.legacy_seq2seq.sequence_loss_by_example(
                                [logits],
                                [tf.reshape(input_.targets, [-1])],
                                [tf.ones([batch_size * num_steps], dtype=data_type())])
                self._cost = cost = tf.reduce_sum(loss) / batch_size
                self._final_state = state
        
                if not is_training:
                        return
        
                self._lr = tf.Variable(0.0, trainable=False)
                tvars = tf.trainable_variables()
                grads, _ = tf.clip_by_global_norm(tf.gradients(cost, tvars),
                                                                                    config.max_grad_norm)
                optimizer = tf.train.GradientDescentOptimizer(self._lr)
                self._train_op = optimizer.apply_gradients(
                        zip(grads, tvars),
                        global_step=tf.contrib.framework.get_or_create_global_step())
        
                self._new_lr = tf.placeholder(tf.float32, shape=[], name="new_learning_rate")
                self._lr_update = tf.assign(self._lr, self._new_lr)

        def assign_lr(self, session, lr_value):
                session.run(self._lr_update, feed_dict={self._new_lr: lr_value})
        
        @property
        def input(self):
                return self._input
        
        @property
        def initial_state(self):
                return self._initial_state
        
        @property
        def cost(self):
                return self._cost
        
        @property
        def final_state(self):
                return self._final_state
        
        @property
        def lr(self):
                return self._lr
        
        @property
        def train_op(self):
                return self._train_op


class SmallConfig(object):

        init_scale = 0.05
        learning_rate = 0.1
        max_grad_norm = 5
        num_layers = 1
        num_steps = 35
        hidden_size = 650
        max_epoch = 6
        max_max_epoch = 39
        keep_prob = 0.5
        lr_decay = 0.8
        batch_size = 20
        vocab_size = 10000
        periods = [1,2,4,8,16]


# this config results the same as a sample RNN
class RNNConfig(object):

        init_scale = 0.05
        learning_rate = 0.1
        max_grad_norm = 5
        num_layers = 1
        num_steps = 35
        hidden_size = 650
        max_epoch = 6
        max_max_epoch = 39
        keep_prob = 0.5
        lr_decay = 0.8
        batch_size = 20
        vocab_size = 10000
        periods = [1]


class MediumConfig(object):
        init_scale = 0.05
        learning_rate = 1.0
        max_grad_norm = 5
        num_layers = 2
        num_steps = 35
        hidden_size = 650
        max_epoch = 6
        max_max_epoch = 39
        keep_prob = 0.5
        lr_decay = 0.8
        batch_size = 20
        vocab_size = 10000


class LargeConfig(object):
        init_scale = 0.04
        learning_rate = 1.0
        max_grad_norm = 10
        num_layers = 2
        num_steps = 35
        hidden_size = 1500
        max_epoch = 14
        max_max_epoch = 55
        keep_prob = 0.35
        lr_decay = 1 / 1.15
        batch_size = 20
        vocab_size = 10000


class TestConfig(object):
        init_scale = 0.1
        learning_rate = 1.0
        max_grad_norm = 1
        num_layers = 1
        num_steps = 2
        hidden_size = 2
        max_epoch = 1
        max_max_epoch = 1
        keep_prob = 1.0
        lr_decay = 0.5
        batch_size = 20
        vocab_size = 10000


def run_epoch(session, model, eval_op=None, verbose=False):
        start_time = time.time()
        costs = 0.0
        iters = 0
        state = session.run(model.initial_state)
        
        fetches = {
                "cost": model.cost,
                "final_state": model.final_state,
        }
        if eval_op is not None:
                fetches["eval_op"] = eval_op

        for step in range(model.input.epoch_size):
                feed_dict = {model.initial_state:state}
                # for i, (c, h) in enumerate(model.initial_state):
                #     feed_dict[c] = state[i].c
                #     feed_dict[h] = state[i].h


                vals = session.run(fetches, feed_dict)
                cost = vals["cost"]
                state = vals["final_state"]
        
                costs += cost
                iters += model.input.num_steps
                
                if verbose and step % (model.input.epoch_size // 10) == 10:
                        print("%.3f perplexity: %.3f speed: %.0f wps" % 
                                (step * 1.0 / model.input.epoch_size, np.exp(costs / iters),
                                 iters * model.input.batch_size / (time.time() - start_time)))
                        sys.stdout.flush()

        return np.exp(costs / iters)


def get_config():
        if FLAGS.model == "small":
                return SmallConfig()
        elif FLAGS.model == "medium":
                return MediumConfig()
        elif FLAGS.model == "large":
                return LargeConfig()
        elif FLAGS.model == "test":
                return TestConfig()
        else:
                raise ValueError("Invalid model: %s", FLAGS.model)


def train():
        if not FLAGS.data_path:
                raise ValueError("Must set --data_path to PTB data directory")
        
        raw_data = reader.ptb_raw_data(FLAGS.data_path)
        train_data, valid_data, test_data, _ = raw_data
        
        config = get_config()
        eval_config = get_config()
        #eval_config.batch_size = 1
        eval_config.num_steps = 1

        with tf.Graph().as_default():

                tf.set_random_seed(0)

                initializer = tf.random_uniform_initializer(-config.init_scale, config.init_scale, seed=0)
                
                with tf.name_scope("Train"):
                        train_input = PTBInput(config=config, data=train_data, name="TrainInput")
                        with tf.variable_scope("Model", reuse=None, initializer=initializer):
                                m = PTBModel(is_training=True, config=config, input_=train_input)
                                tf.summary.scalar("Training Loss", m.cost)
                                tf.summary.scalar("Learning Rate", m.lr)
                
                with tf.name_scope("Valid"):
                        valid_input = PTBInput(config=config, data=valid_data, name="ValidInput")
                        with tf.variable_scope("Model", reuse=True, initializer=initializer):
                                mvalid = PTBModel(is_training=False, config=config, input_=valid_input)
                                tf.summary.scalar("Validation Loss", mvalid.cost)
                
                with tf.name_scope("Test"):
                        test_input = PTBInput(config=eval_config, data=test_data, name="TestInput")
                        with tf.variable_scope("Model", reuse=True, initializer=initializer):
                                mtest = PTBModel(is_training=False, config=eval_config,
                                                                 input_=test_input)

                sv = tf.train.Supervisor(logdir=FLAGS.save_path)
                with sv.managed_session() as session:
                        for i in range(config.max_max_epoch):
                                lr_decay = config.lr_decay ** max(i + 1 - config.max_epoch, 0.0)
                                m.assign_lr(session, config.learning_rate * lr_decay)
                                
                                print("Epoch: %d Learning rate: %.3f" % (i + 1, session.run(m.lr)))
                                train_perplexity = run_epoch(session, m, eval_op=m.train_op,
                                                                                         verbose=True)
                                print("Epoch: %d Train Perplexity: %.3f" % (i + 1, train_perplexity))
                                valid_perplexity = run_epoch(session, mvalid)
                                print("Epoch: %d Valid Perplexity: %.3f" % (i + 1, valid_perplexity))
                        
                        test_perplexity = run_epoch(session, mtest)
                        print("Test Perplexity: %.3f" % test_perplexity)
                        
                        if FLAGS.save_path:
                                print("Saving model to %s." % FLAGS.save_path)
                                sv.saver.save(session, FLAGS.save_path, global_step=sv.global_step)


if __name__ == "__main__":
        train()
