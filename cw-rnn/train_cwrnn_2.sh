#!/bin/bash
#SBATCH --ntasks=1
#SBATCH --gres=gpu:1
#SBATCH --time=10-00:00
#SBATCH --mem-per-cpu=10000
#SBATCH --output=train_cwrnn_16.out	#job name and job id
#SBATCH --job-name=train_cwrnn_16



python /scratch/jzhang/tools/write_rc.py
source ~/.bashrc

python cwrnn_ptb_word_lm.py


