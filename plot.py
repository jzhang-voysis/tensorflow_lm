import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np


V='Valid Perplexity'
T='Train Perplexity'
def reader(f):
    t_ppls = []
    v_ppls = []
    
    
    for line in open(f):
        if V in line:
            v_ppls.append(line.split(' ')[-1])
        elif T in line:
            t_ppls.append(line.split(' ')[-1])
    return np.asarray(t_ppls, np.float32), np.asarray(v_ppls, np.float32)


lstm_t_ppls, lstm_v_ppls = reader("lstm/train_lstm.out")
rnn_t_ppls, rnn_v_ppls = reader("rnn/train_rnn.out")
cwnn_t_ppls, cwrnn_v_ppls = reader("cw-rnn/train_cwrnn_16.out")


plt.figure()
lstm_t_ppls, = plt.plot(range(1, len(lstm_t_ppls)+1), lstm_t_ppls, label='LSTM Training', marker="o")
cwnn_t_ppls, = plt.plot(range(1, len(cwnn_t_ppls)+1), cwnn_t_ppls, label='CW-RNN Training', marker="^")
rnn_t_ppls, = plt.plot(range(1, len(rnn_t_ppls)+1), rnn_t_ppls, label='RNN Training', marker="v")
lstm_v_ppls, = plt.plot(range(1, len(lstm_v_ppls)+1), lstm_v_ppls, label='LSTM Valid', marker="*")
rnn_v_ppls, = plt.plot(range(1, len(rnn_v_ppls)+1), rnn_v_ppls, label='RNN Valid', marker=">")
cwrnn_v_ppls, = plt.plot(range(1, len(cwrnn_v_ppls)+1), cwrnn_v_ppls, label='CW-RNN Valid',marker="<")



plt.xlabel("Epoch")
plt.ylabel("Perplexity")
plt.legend(handles=[lstm_t_ppls, cwnn_t_ppls, rnn_t_ppls, lstm_v_ppls, cwrnn_v_ppls, rnn_v_ppls])

plt.savefig("plot.png", bbox_inches='tight')
plt.close('all')