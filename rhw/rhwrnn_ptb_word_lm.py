import time
import sys
import numpy as np
import tensorflow as tf
import reader
from tensorflow.python.ops import math_ops
from tensorflow.python.ops.rnn_cell_impl import RNNCell
from tensorflow.python.util import nest
from tensorflow.python.ops import variable_scope as vs
from tensorflow.python.ops import array_ops
from tensorflow.python.ops import init_ops
from tensorflow.python.ops import nn_ops
_BIAS_VARIABLE_NAME = "bias"
_WEIGHTS_VARIABLE_NAME = "kernel"

flags = tf.flags
logging = tf.logging

flags.DEFINE_string(
    "model", "small",
    "A type of model. Possible options are: small, medium, large.")
flags.DEFINE_string("data_path", "../data",
                    "Where the training/test data is stored.")
flags.DEFINE_string("save_path", None,
                    "Model output directory.")
flags.DEFINE_bool("use_fp16", False,
                  "Train using 16-bit floats instead of 32bit floats")

FLAGS = flags.FLAGS


def data_type():
        return tf.float16 if FLAGS.use_fp16 else tf.float32


class RHNCell(RNNCell):
    """Variational Recurrent Highway Layer

    Reference: https://arxiv.org/abs/1607.03474
    """

    def __init__(self, num_units, in_size, depth=10, forget_bias=None):
        self._num_units = num_units
        self._in_size = in_size
        self.depth = depth
        self.forget_bias = forget_bias

    @property
    def input_size(self):
        return self._in_size

    @property
    def output_size(self):
        return self._num_units

    @property
    def state_size(self):
        return self._num_units

    def step(self, inputs, state, scope=None):
        current_state = state[0]
        noise_i = state[1]
        noise_h = state[2]
        for i in range(self.depth):
            with tf.variable_scope('h_'+str(i)):
                if i == 0:
                    h = tf.tanh(linear([inputs * noise_i, current_state * noise_h], self._num_units, True))
                else:
                    h = tf.tanh(linear([current_state * noise_h], self._num_units, True))
            with tf.variable_scope('t_'+str(i)):
                if i == 0:
                    t = tf.sigmoid(linear([inputs * noise_i, current_state * noise_h], self._num_units, True, self.forget_bias))
                else:
                    t = tf.sigmoid(linear([current_state * noise_h], self._num_units, True, self.forget_bias))
            current_state = (h - current_state)* t + current_state

        return current_state, [current_state, noise_i, noise_h]


def linear(args, output_size, bias, bias_start=None, scope=None):
    
    if args is None or (nest.is_sequence(args) and not args):
        raise ValueError("`args` must be specified")
    if not nest.is_sequence(args):
        args = [args]

    # Calculate the total size of arguments on dimension 1.
    total_arg_size = 0
    shapes = [a.get_shape().as_list() for a in args]
    for shape in shapes:
        if len(shape) != 2:
            raise ValueError("Linear is expecting 2D arguments: %s" % str(shapes))
        if not shape[1]:
            raise ValueError("Linear expects shape[1] of arguments: %s" % str(shapes))
        else:
            total_arg_size += shape[1]

    dtype = [a.dtype for a in args][0]

    # Now the computation.
    with vs.variable_scope(scope or "Linear"):
        matrix = vs.get_variable(
                "Matrix", [total_arg_size, output_size], dtype=dtype)
        if len(args) == 1:
            res = math_ops.matmul(args[0], matrix)
        else:
            res = math_ops.matmul(array_ops.concat(args, 1), matrix)
        if not bias:
            return res
        elif bias_start is None:
            bias_term = vs.get_variable("Bias", [output_size], dtype=dtype)
        else:
            bias_term = vs.get_variable("Bias", [output_size], dtype=dtype,
                                                                    initializer=tf.constant_initializer(bias_start, dtype=dtype))
    return res + bias_term


class PTBInput(object):

    def __init__(self, config, data, name=None):
        self.batch_size = batch_size = config.batch_size
        self.num_steps = num_steps = config.num_steps
        self.epoch_size = ((len(data) // batch_size) - 1) // num_steps
        self.input_data, self.targets = reader.ptb_producer(data, batch_size, num_steps, name=name)


class PTBModel(object):

    def __init__(self, is_training, config, input_):
        self._input = input_
    
        batch_size = input_.batch_size
        num_steps = input_.num_steps
        size = config.hidden_size
        vocab_size = config.vocab_size
        

        # Slightly better results can be obtained with forget gate biases
        # initialized to 1 but the hyperparameters of the model would need to be
        # different than reported in the paper.
        rnn_cell = RHNCell(size, size, forget_bias=config.init_bias)
        #if is_training and config.keep_prob < 1:
        #   lstm_cell = tf.nn.rnn_cell.DropoutWrapper(lstm_cell, output_keep_prob=config.keep_prob)
        #cell = tf.nn.rnn_cell.MultiRNNCell([lstm_cell] * config.num_layers, state_is_tuple=True)
    
        self._noise_i = tf.placeholder(tf.float32, [batch_size, size])
        self._noise_h = tf.placeholder(tf.float32, [batch_size, size])


        self._initial_state = rnn_cell.zero_state(batch_size, data_type())
    
        print self._initial_state
        with tf.device("/cpu:0"):
            embedding = tf.get_variable("embedding", [vocab_size, size], dtype=data_type(), initializer=tf.contrib.layers.xavier_initializer(seed=0))
            inputs = tf.nn.embedding_lookup(embedding, input_.input_data)
    
        if is_training and config.keep_prob < 1:
            inputs = tf.nn.dropout(inputs, config.keep_prob, seed=0)
    
        
        outputs = []
        state = self._initial_state
        packed_state = [state, self._noise_i, self._noise_h]
        with tf.variable_scope("RNN"):
            for time_step in range(num_steps):
                if time_step > 0: 
                    tf.get_variable_scope().reuse_variables()
                (cell_output, packed_state) = rnn_cell.step(inputs[:, time_step, :], packed_state)
                outputs.append(cell_output)
    
        output = tf.reshape(tf.concat(outputs, 1), [-1, size])
        softmax_w = tf.get_variable(
            "softmax_w", [size, vocab_size], dtype=data_type(), initializer=tf.contrib.layers.xavier_initializer(seed=0))
        softmax_b = tf.get_variable("softmax_b", [vocab_size], dtype=data_type(), initializer=tf.contrib.layers.xavier_initializer(seed=0))
        logits = tf.matmul(output, softmax_w) + softmax_b
        loss = tf.contrib.legacy_seq2seq.sequence_loss_by_example(
                [logits],
                [tf.reshape(input_.targets, [-1])],
                [tf.ones([batch_size * num_steps], dtype=data_type())])
        pred_loss = tf.reduce_sum(loss) / batch_size

        tvars = tf.trainable_variables()
        l2_loss = tf.add_n([tf.nn.l2_loss(v) for v in tvars])

        self._cost = cost = pred_loss + config.weight_decay * l2_loss


        self._final_state = state
    
        if not is_training:
            return
    
        self._lr = tf.Variable(0.0, trainable=False)
        tvars = tf.trainable_variables()
        grads, _ = tf.clip_by_global_norm(tf.gradients(cost, tvars),
                                          config.max_grad_norm)
        optimizer = tf.train.GradientDescentOptimizer(self._lr)
        self._train_op = optimizer.apply_gradients(
            zip(grads, tvars),
            global_step=tf.contrib.framework.get_or_create_global_step())
    
        self._new_lr = tf.placeholder(tf.float32, shape=[], name="new_learning_rate")
        self._lr_update = tf.assign(self._lr, self._new_lr)

    def assign_lr(self, session, lr_value):
        session.run(self._lr_update, feed_dict={self._new_lr: lr_value})
    
    @property
    def input(self):
        return self._input

    @property
    def noise_i(self):
        return self._noise_i

    @property
    def noise_h(self):
        return self._noise_h

    @property
    def initial_state(self):
        return self._initial_state
    
    @property
    def cost(self):
        return self._cost
    
    @property
    def final_state(self):
        return self._final_state
    
    @property
    def lr(self):
        return self._lr
    
    @property
    def train_op(self):
        return self._train_op


class SmallConfig(object):


    # init_scale = 0.05
    # learning_rate = 0.1
    # max_grad_norm = 5
    # num_layers = 1
    # num_steps = 35
    # hidden_size = 650
    # max_epoch = 6
    # max_max_epoch = 39
    # keep_prob = 0.5
    # lr_decay = 0.8
    # batch_size = 20
    # vocab_size = 10000

    init_scale = 0.04
    learning_rate = 0.2
    max_grad_norm = 10
    num_layers = 1
    num_steps = 35
    hidden_size = 1000
    max_epoch = 16
    max_max_epoch = 500
    keep_prob = 0.5
    lr_decay = 0.97
    batch_size = 20
    vocab_size = 10000
    weight_decay = 1e-7
    init_bias = -2.0

    drop_x = 0.10
    drop_i = 0.40
    drop_h = 0.10
    drop_o = 0.40



class MediumConfig(object):
    init_scale = 0.05
    learning_rate = 1.0
    max_grad_norm = 5
    num_layers = 2
    num_steps = 35
    hidden_size = 650
    max_epoch = 6
    max_max_epoch = 39
    keep_prob = 0.5
    lr_decay = 0.8
    batch_size = 20
    vocab_size = 10000


class LargeConfig(object):
    init_scale = 0.04
    learning_rate = 1.0
    max_grad_norm = 10
    num_layers = 2
    num_steps = 35
    hidden_size = 1500
    max_epoch = 14
    max_max_epoch = 55
    keep_prob = 0.35
    lr_decay = 1 / 1.15
    batch_size = 20
    vocab_size = 10000


class TestConfig(object):
    init_scale = 0.1
    learning_rate = 1.0
    max_grad_norm = 1
    num_layers = 1
    num_steps = 2
    hidden_size = 2
    max_epoch = 1
    max_max_epoch = 1
    keep_prob = 1.0
    lr_decay = 0.5
    batch_size = 20
    vocab_size = 10000


def run_epoch(session, model, config, eval_op=None, verbose=False):
    start_time = time.time()
    costs = 0.0
    iters = 0
    state = session.run(model.initial_state)
    
    fetches = {
        "cost": model.cost,
        "final_state": model.final_state,
    }
    if eval_op is not None:
        fetches["eval_op"] = eval_op

    for step in range(model.input.epoch_size):

        noise_i, noise_h, noise_o = get_noise(config, config.drop_x, config.drop_i, config.drop_h, config.drop_o)

        #print state.shape, noise_i.shape, noise_h.shape, noise_o.shape
        feed_dict = {model.initial_state:state,
                    model.noise_i: noise_i, 
                    model.noise_h: noise_h}
        # for i, (c, h) in enumerate(model.initial_state):
        #    feed_dict[c] = state[i].c
        #    feed_dict[h] = state[i].h


        vals = session.run(fetches, feed_dict)
        cost = vals["cost"]
        state = vals["final_state"]
    
        #print "????????????????????", cost
        costs += cost
        iters += model.input.num_steps
        
        if verbose and step % (model.input.epoch_size // 10) == 10:
            print("%.3f perplexity: %.3f speed: %.0f wps" % 
                (step * 1.0 / model.input.epoch_size, np.exp(costs / iters),
                 iters * model.input.batch_size / (time.time() - start_time)))
            sys.stdout.flush()

    return np.exp(costs / iters)


def get_config():
    if FLAGS.model == "small":
        return SmallConfig()
    elif FLAGS.model == "medium":
        return MediumConfig()
    elif FLAGS.model == "large":
        return LargeConfig()
    elif FLAGS.model == "test":
        return TestConfig()
    else:
        raise ValueError("Invalid model: %s", FLAGS.model)

def get_noise(config, drop_x, drop_i, drop_h, drop_o):
    keep_x, keep_i, keep_h, keep_o = 1.0 - drop_x, 1.0 - drop_i, 1.0 - drop_h, 1.0 - drop_o
    

    if keep_i < 1.0:
        noise_i = (np.random.random_sample((config.batch_size, config.hidden_size)) < keep_i).astype(np.float32) / keep_i
    else:
        noise_i = np.ones((config.batch_size, config.hidden_size), dtype=np.float32)
    if keep_h < 1.0:
        noise_h = (np.random.random_sample((config.batch_size, config.hidden_size)) < keep_h).astype(np.float32) / keep_h
    else:
        noise_h = np.ones((config.batch_size, config.hidden_size), dtype=np.float32)
    if keep_o < 1.0:
        noise_o = (np.random.random_sample((config.batch_size, 1, config.hidden_size)) < keep_o).astype(np.float32) / keep_o
    else:
        noise_o = np.ones((config.batch_size, 1, config.hidden_size), dtype=np.float32)
    return noise_i, noise_h, noise_o


def train():
    if not FLAGS.data_path:
        raise ValueError("Must set --data_path to PTB data directory")
    
    raw_data = reader.ptb_raw_data(FLAGS.data_path)
    train_data, valid_data, test_data, _ = raw_data
    
    config = get_config()
    eval_config = get_config()
    #eval_config.batch_size = 1
    eval_config.num_steps = 1

    with tf.Graph().as_default():

        tf.set_random_seed(0)

        initializer = tf.random_uniform_initializer(-config.init_scale,
                                                    config.init_scale, seed=0)
        
        with tf.name_scope("Train"):
            train_input = PTBInput(config=config, data=train_data, name="TrainInput")
            with tf.variable_scope("Model", reuse=None, initializer=initializer):
                m = PTBModel(is_training=True, config=config, input_=train_input)
                tf.summary.scalar("Training Loss", m.cost)
                tf.summary.scalar("Learning Rate", m.lr)
        
        with tf.name_scope("Valid"):
            valid_input = PTBInput(config=config, data=valid_data, name="ValidInput")
            with tf.variable_scope("Model", reuse=True, initializer=initializer):
                mvalid = PTBModel(is_training=False, config=config, input_=valid_input)
                tf.summary.scalar("Validation Loss", mvalid.cost)
        
        with tf.name_scope("Test"):
            test_input = PTBInput(config=eval_config, data=test_data, name="TestInput")
            with tf.variable_scope("Model", reuse=True, initializer=initializer):
                mtest = PTBModel(is_training=False, config=eval_config,
                                 input_=test_input)

        sv = tf.train.Supervisor(logdir=FLAGS.save_path)
        with sv.managed_session() as session:
            for i in range(config.max_max_epoch):
                lr_decay = config.lr_decay ** max(i + 1 - config.max_epoch, 0.0)
                m.assign_lr(session, config.learning_rate * lr_decay)
                
                print("Epoch: %d Learning rate: %.3f" % (i + 1, session.run(m.lr)))
                train_perplexity = run_epoch(session, m, config, eval_op=m.train_op,
                                             verbose=True)
                print("Epoch: %d Train Perplexity: %.3f" % (i + 1, train_perplexity))
                valid_perplexity = run_epoch(session, mvalid, config)
                print("Epoch: %d Valid Perplexity: %.3f" % (i + 1, valid_perplexity))
            
            test_perplexity = run_epoch(session, mtest, config)
            print("Test Perplexity: %.3f" % test_perplexity)
            
            if FLAGS.save_path:
                print("Saving model to %s." % FLAGS.save_path)
                sv.saver.save(session, FLAGS.save_path, global_step=sv.global_step)


if __name__ == "__main__":
    train()

