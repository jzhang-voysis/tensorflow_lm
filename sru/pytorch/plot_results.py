import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

def read_test_ppl_results(f, max = 300):
    r = []
    for line in open(f):
        line = line.strip()
        if "dev_ppl=" in line:
            r.append(float(line.split("\t")[0].split(" ")[-1].replace("dev_ppl=", "")))
    print f, len(r)
    return r[0:max]

'''
def read_test_ppl_results(f, max = 300):
    r = []
    for line in open(f):
        line = line.strip()
        if "test_ppl=" in line:
            r.append(float(line.split("\t")[0].split(" ")[-1].replace("test_ppl=", "")))
    print f, len(r)
    return r[0:max]
'''

fig = plt.figure(figsize=(15, 15))
fig.suptitle('SRU/LSTM PTB LM PPL on dev data')


lstm_test_plot = read_test_ppl_results("lstm_torch_ptb_baseline.out")
sru_test_plot = read_test_ppl_results("sru_torch_ptb_baseline.out")

ax1 = fig.add_subplot(221)
ax1.title.set_text('Experiment 1, all default parameters')
lstm_test, = ax1.plot(range(1,len(lstm_test_plot)+1),lstm_test_plot, label='LSTM (test)')
sru_test, = ax1.plot(range(1,len(sru_test_plot)+1),sru_test_plot, label='SRU (test)')
ax1.set_xlabel("Epoch")
ax1.set_ylabel("PPL")
ax1.legend(handles=[lstm_test, sru_test])

lstm_test_plot = read_test_ppl_results("lstm_h350_torch_ptb_baseline.out")
sru_test_plot = read_test_ppl_results("sru_h350_torch_ptb_baseline.out")
ax1 = fig.add_subplot(222)
ax1.title.set_text('Experiment 2, hidden 350, other parameters are default')
lstm, = ax1.plot(range(1,len(lstm_test_plot)+1),lstm_test_plot, label='LSTM (test)')
sru, = ax1.plot(range(1,len(sru_test_plot)+1),sru_test_plot, label='SRU (test)')
ax1.set_xlabel("Epoch")
ax1.set_ylabel("PPL")
ax1.legend(handles=[lstm, sru])

lstm_test_plot = read_test_ppl_results("lstm_depth1_torch_ptb_baseline.out")
sru_test_plot = read_test_ppl_results("sru_depth1_torch_ptb_baseline.out")
ax1 = fig.add_subplot(223)
ax1.title.set_text('Experiment 3, 1 layer SRU/LSTM, other parameters are default')
lstm, = ax1.plot(range(1,len(lstm_test_plot)+1),lstm_test_plot, label='LSTM (test)')
sru, = ax1.plot(range(1,len(sru_test_plot)+1),sru_test_plot, label='SRU (test)')
ax1.set_xlabel("Epoch")
ax1.set_ylabel("PPL")
ax1.legend(handles=[lstm, sru])

lstm_test_plot = read_test_ppl_results("lstm_depth1_h600_torch_ptb_baseline.out")
sru_test_plot = read_test_ppl_results("sru_depth1_h600_torch_ptb_baseline.out")
ax1 = fig.add_subplot(224)
ax1.title.set_text('Experiment 4, 1 layer SRU/LSTM, hidden 600, other parameters are default')
lstm, = ax1.plot(range(1,len(lstm_test_plot)+1),lstm_test_plot, label='LSTM (test)')
sru, = ax1.plot(range(1,len(sru_test_plot)+1),sru_test_plot, label='SRU (test)')
ax1.set_xlabel("Epoch")
ax1.set_ylabel("PPL")
ax1.legend(handles=[lstm, sru])


plt.tight_layout(rect=[0, 0.03, 1, 0.95])
plt.savefig("test_results.png", bbox_inches='tight')
plt.close('all')



