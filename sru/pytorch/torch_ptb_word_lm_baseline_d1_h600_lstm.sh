#!/bin/bash
#SBATCH --ntasks=1
#SBATCH --gres=gpu:1
#SBATCH --time=10-00:00
#SBATCH --mem-per-cpu=5000
#SBATCH --output=lstm_depth1_h600_torch_ptb_baseline.out	#job name and job id
#SBATCH --job-name=lstm_depth1_h600_torch_ptb_baseline



python /scratch/jzhang/tools/write_rc.py
source ~/.bashrc

python torch_ptb_word_lm.py --train ../../data/ptb.train.txt \
			    --dev ../../data/ptb.valid.txt \
			    --test ../../data/ptb.test.txt \
			    --depth 1 \
			    --d 600 \
			    --lstm


