import glob, os
def get_statis(f):
    dev_ppl = []
    train_ppl = []
    test_ppl = []
    epoch = []
    size = None
    for line in open(f):
        line = line.strip()
        if "dev_ppl=" in line:
            dev_ppl.append(float(line.split("\t")[0].split(" ")[-1].replace("dev_ppl=", "")))
        if "train_ppl=" in line:
            train_ppl.append(float(line.split("\t")[0].split(" ")[-3].replace("train_ppl=", "")))
        if "test_ppl=" in line:
            test_ppl.append(float(line.split("\t")[0].split(" ")[-1].replace("test_ppl=", "")))
        if "num of parameters:" in line:
            size = line.split(" ")[-1]
        
    print "Size = {}\nTrain_ppl = {}\nDev_ppl = {}\nTest_ppl = {}\nTotal_epoch = {}".format(size, train_ppl[-1], dev_ppl[-1], test_ppl[-1], len(dev_ppl))
for file in glob.glob("*.out"):
    print file
    get_statis(file)