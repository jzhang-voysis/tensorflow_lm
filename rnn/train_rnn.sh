#!/bin/bash
#SBATCH --ntasks=1
#SBATCH --gres=gpu:1
#SBATCH --time=10-00:00
#SBATCH --mem-per-cpu=10000
#SBATCH --output=train_rnn.out	#job name and job id
#SBATCH --job-name=train_rnn



python /scratch/jzhang/tools/write_rc.py
source ~/.bashrc

python rnn_ptb_word_lm.py


