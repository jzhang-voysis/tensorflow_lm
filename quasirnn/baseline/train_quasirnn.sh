#!/bin/bash
#SBATCH --ntasks=1
#SBATCH --gres=gpu:1
#SBATCH --time=10-00:00
#SBATCH --mem-per-cpu=8000
#SBATCH --output=baseline_train_quasirnn.out	#job name and job id
#SBATCH --job-name=baseline_train_quasirnn



python /scratch/jzhang/tools/write_rc.py
source ~/.bashrc

python train_lm.py --zoneout 0.1 --dropout 0 --data_dir ../../data/


