import time
import sys
import numpy as np
import tensorflow as tf
import reader
import numbers
from tensorflow.contrib.layers import xavier_initializer
from tensorflow.python.framework import tensor_shape
from tensorflow.python.framework import tensor_util
from tensorflow.python.ops import array_ops
from tensorflow.python.ops import random_ops
from tensorflow.python.ops import math_ops
from tensorflow.python.framework import ops


#https://raw.githubusercontent.com/santi-pdp/quasi-rnn/master/qrnn.py
def zoneout(x, keep_prob, noise_shape=None, seed=None, name=None):
    """Computes zoneout (including dropout without scaling).
    With probability `keep_prob`.
    By default, each element is kept or dropped independently.  If `noise_shape`
    is specified, it must be
    [broadcastable](http://docs.scipy.org/doc/numpy/user/basics.broadcasting.html)
    to the shape of `x`, and only dimensions with `noise_shape[i] == shape(x)[i]`
    will make independent decisions.  For example, if `shape(x) = [k, l, m, n]`
    and `noise_shape = [k, 1, 1, n]`, each batch and channel component will be
    kept independently and each row and column will be kept or not kept together.
    Args:
      x: A tensor.
      keep_prob: A scalar `Tensor` with the same type as x. The probability
        that each element is kept.
      noise_shape: A 1-D `Tensor` of type `int32`, representing the
        shape for randomly generated keep/drop flags.
      seed: A Python integer. Used to create random seeds. See
        [`set_random_seed`](../../api_docs/python/constant_op.md#set_random_seed)
        for behavior.
      name: A name for this operation (optional).
    Returns:
      A Tensor of the same shape of `x`.
    Raises:
      ValueError: If `keep_prob` is not in `(0, 1]`.
    """
    with tf.name_scope(name or "dropout") as name:
        x = ops.convert_to_tensor(x, name="x")
        if isinstance(keep_prob, numbers.Real) and not 0 < keep_prob <= 1:
            raise ValueError("keep_prob must be a scalar tensor or a float in the "
                             "range (0, 1], got %g" % keep_prob)
        keep_prob = ops.convert_to_tensor(keep_prob,
                                        dtype=x.dtype,
                                        name="keep_prob")
        keep_prob.get_shape().assert_is_compatible_with(tensor_shape.scalar())

        # Do nothing if we know keep_prob == 1
        if tensor_util.constant_value(keep_prob) == 1:
            return x

        noise_shape = noise_shape if noise_shape is not None else array_ops.shape(x)
        # uniform [keep_prob, 1.0 + keep_prob)
        random_tensor = keep_prob
        random_tensor += random_ops.random_uniform(noise_shape,
                                                 seed=seed,
                                                 dtype=x.dtype)
        # 0. if [keep_prob, 1.0) and 1. if [1.0, 1.0 + keep_prob)
        binary_tensor = math_ops.floor(random_tensor)
        ret = x * binary_tensor
        ret.set_shape(x.get_shape())
        return 1. - ret


class QRNN_pooling(tf.nn.rnn_cell.RNNCell):

    def __init__(self, out_fmaps, pool_type):
        self.__pool_type = pool_type
        self.__out_fmaps = out_fmaps

    @property
    def state_size(self):
        return self.__out_fmaps

    @property
    def output_size(self):
        return self.__out_fmaps

    def __call__(self, inputs, state, scope=None):
        """
        inputs: 2-D tensor of shape [batch_size, Zfeats + [gates]]
        """
        pool_type = self.__pool_type
        # print('QRNN pooling inputs shape: ', inputs.get_shape())
        # print('QRNN pooling state shape: ', state.get_shape())
        with tf.variable_scope(scope or "QRNN-{}-pooling".format(pool_type)):
            if pool_type == 'f':
                # extract Z activations and F gate activations
                Z, F = tf.split(inputs, 2, axis=1)
                # return the dynamic average pooling
                output = tf.multiply(F, state) + tf.multiply(tf.subtract(1., F), Z)
                return output, output
            elif pool_type == 'fo':
                # extract Z, F gate and O gate
                Z, F, O = tf.split(inputs, 3, axis=1)
                new_state = tf.multiply(F, state) + tf.multiply(tf.subtract(1., F), Z)
                output = tf.multiply(O, new_state)
                return output, new_state
            elif pool_type == 'ifo':
                # extract Z, I gate, F gate, and O gate
                Z, I, F, O = tf.split(inputs, 4, axis=1)
                new_state = tf.multiply(F, state) + tf.multiply(I, Z)
                output = tf.multiply(O, new_state)
                return output, new_state
            else:
                raise ValueError('Pool type must be either f, fo or ifo')



class QRNN_layer(object):
    """ Quasi-Recurrent Neural Network Layer
        (cf. https://arxiv.org/abs/1611.01576)
    """
    def __init__(self, out_fmaps, fwidth=2,
                 activation=tf.tanh, pool_type='fo', zoneout=0.1, infer=False,
                 bias_init_val=None,
                 name='QRNN'):
        """
        pool_type: can be f, fo, or ifo
        zoneout: > 0 means apply zoneout with p = 1 - zoneout
        bias_init_val: by default there is no bias.
        """
        self.out_fmaps = out_fmaps
        self.activation = activation
        self.name = name
        self.infer = infer
        self.pool_type = pool_type
        self.fwidth = fwidth
        self.out_fmaps = out_fmaps
        self.zoneout = zoneout
        self.bias_init_val = bias_init_val

    def __call__(self, input_):
        input_shape = input_.get_shape().as_list()
        batch_size = input_shape[0]
        fwidth = self.fwidth
        out_fmaps = self.out_fmaps
        pool_type = self.pool_type
        zoneout = self.zoneout
        with tf.variable_scope(self.name):
            # gates: list containing gate activations (num of gates depending
            # on pool_type)
            Z, gates = self.convolution(input_, fwidth, out_fmaps, pool_type,
                                        zoneout)
            # join all features (Z and gates) into Tensor at dim 2 merged
            T = tf.concat([Z] + gates, axis=2)
            # create the pooling layer
            pooling = QRNN_pooling(out_fmaps, pool_type)
            self.initial_state = pooling.zero_state(batch_size=batch_size,
                                                    dtype=tf.float32)
            # encapsulate the pooling in the iterative dynamic_rnn
            H, last_C = tf.nn.dynamic_rnn(pooling, T,
                                          initial_state=self.initial_state)
            self.Z = Z
            return H, last_C

    def convolution(self, input_, filter_width, out_fmaps, pool_type, zoneout_):
        """ Applies 1D convolution along time-dimension (T) assuming input
            tensor of dim (batch_size, T, n) and returns
            (batch_size, T, out_fmaps)
            zoneout: regularization (dropout) of F gate
        """
        in_shape = input_.get_shape()
        in_fmaps = in_shape[-1]
        num_gates = len(pool_type)
        gates = []
        # pad on the left to mask the convolution (make it causal)
        pinput = tf.pad(input_, [[0, 0], [filter_width - 1, 0], [0, 0]])
        with tf.variable_scope('convolutions'):
            Wz = tf.get_variable('Wz', [filter_width, in_fmaps, out_fmaps],
                                 initializer=tf.random_uniform_initializer(minval=-.05, maxval=.05))
            z_a = tf.nn.conv1d(pinput, Wz, stride=1, padding='VALID')
            if self.bias_init_val is not None:
                bz = tf.get_variable('bz', [out_fmaps],
                                     initializer=tf.constant_initializer(0.))
                z_a += bz

            z = self.activation(z_a)
            # compute gates convolutions
            for gate_name in pool_type:
                Wg = tf.get_variable('W{}'.format(gate_name),
                                     [filter_width, in_fmaps, out_fmaps],
                                     initializer=tf.random_uniform_initializer(minval=-.05, maxval=.05))
                g_a = tf.nn.conv1d(pinput, Wg, stride=1, padding='VALID')
                if self.bias_init_val is not None:
                    bg = tf.get_variable('b{}'.format(gate_name), [out_fmaps],
                                         initializer=tf.constant_initializer(0.))
                    g_a += bg
                g = tf.sigmoid(g_a)
                if not self.infer and zoneout_ > 0 and gate_name == 'f':
                    print('Applying zoneout {} to gate F'.format(zoneout_))
                    # appy zoneout to F
                    g = zoneout((1. - g), 1. - zoneout_)
                    # g = 1. - tf.nn.dropout((1. - g), 1. - zoneout)
                gates.append(g)
        return z, gates



flags = tf.flags
logging = tf.logging

flags.DEFINE_string(
    "model", "small",
    "A type of model. Possible options are: small, medium, large.")
flags.DEFINE_string("data_path", "../data",
                    "Where the training/test data is stored.")
flags.DEFINE_string("save_path", None,
                    "Model output directory.")
flags.DEFINE_bool("use_fp16", False,
                  "Train using 16-bit floats instead of 32bit floats")

FLAGS = flags.FLAGS

def scalar_summary(name, x):
    return tf.summary.scalar(name, x)

def histogram_summary(name, x):
    return tf.summary.histogram(name, x)

def data_type():
    return tf.float16 if FLAGS.use_fp16 else tf.float32

class PTBModel(object):

    def __init__(self, is_training, config, input_):
        self._input = input_
        self.zoneout = 0.1
        batch_size = input_.batch_size
        num_steps = input_.num_steps
        size = config.hidden_size
        vocab_size = config.vocab_size
        embeddings = None
        self.final_state = []
        self.initial_states = []
        self.qrnns = []
        
        self.regularizer = tf.contrib.layers.l2_regularizer(scale=0.0002)

        word_W = tf.get_variable("embedding",
                                     [vocab_size, size],
                                      initializer=tf.random_uniform_initializer(minval=-.05, maxval=.05))
        words = tf.split(tf.expand_dims(input_.input_data, -1), num_steps, axis = 1)
        print 'len of words: ', len(words)
        for word_idx in words:
            word_embed = tf.nn.embedding_lookup(word_W, word_idx)
            if not is_training and config.keep_prob > 0:
                word_embed = tf.nn.dropout(word_embed, config.keep_prob,
                                           name='dout_word_emb')
            #print('word embed shape: ', word_embed.get_shape().as_list())
            
            #concat all embeddings
            if embeddings is None:
                embeddings = tf.squeeze(word_embed, [1])
            else:
                embeddings = tf.concat([embeddings,
                                       tf.squeeze(word_embed, [1])], axis = 1)
        print('word embed shape: ', embeddings.get_shape().as_list())   
        qrnn_h = embeddings
    
        for qrnn_l in range(config.num_layers):
            qrnn_ = QRNN_layer(size, pool_type='fo',
                               zoneout=self.zoneout,
                               name='QRNN_layer{}'.format(qrnn_l),
                               infer=(is_training == 0))
            qrnn_h, last_state = qrnn_(qrnn_h)
            #qrnn_h = qrnn_.h
            # apply dropout if required
            if not is_training and config.keep_prob > 0:
                qrnn_h_f = tf.reshape(qrnn_h, [-1, size])
                qrnn_h_dout = tf.nn.dropout(qrnn_h_f, config.keep_prob,
                                            name='dout_qrnn{}'.format(qrnn_l))
                qrnn_h = tf.reshape(qrnn_h_dout, [batch_size, -1, size])
            #self.last_states.append(qrnn_.last_state)
            self.final_state.append(last_state)
            histogram_summary('qrnn_state_{}'.format(qrnn_l),
                              last_state)
            scalar_summary('qrnn_avg_state_{}'.format(qrnn_l),
                           tf.reduce_mean(last_state))
            self.initial_states.append(qrnn_.initial_state)
            self.qrnns.append(qrnn_)

                    
        output = tf.reshape(qrnn_h, [-1, size])
        softmax_w = tf.get_variable(
            "softmax_w", [size, vocab_size], dtype=data_type())
        softmax_b = tf.get_variable("softmax_b", [vocab_size], dtype=data_type())
        logits = tf.matmul(output, softmax_w) + softmax_b
        loss = tf.contrib.legacy_seq2seq.sequence_loss_by_example(
                [logits],
                [tf.reshape(input_.targets, [-1])],
                [tf.ones([batch_size * num_steps], dtype=data_type())])

        #add l2 loss
        self._lr = tf.Variable(0.0, trainable=False)
        tvars = tf.trainable_variables()
        reg_term = tf.contrib.layers.apply_regularization(self.regularizer, tvars)
        loss += reg_term

        self._cost = cost = tf.reduce_sum(loss) / batch_size
    
        if not is_training:
            return
    
        

        grads, _ = tf.clip_by_global_norm(tf.gradients(cost, tvars),
                                          config.max_grad_norm)
        optimizer = tf.train.GradientDescentOptimizer(self._lr)
        self._train_op = optimizer.apply_gradients(
            zip(grads, tvars),
            global_step=tf.contrib.framework.get_or_create_global_step())
    
        self._new_lr = tf.placeholder(tf.float32, shape=[], name="new_learning_rate")
        self._lr_update = tf.assign(self._lr, self._new_lr)

    def assign_lr(self, session, lr_value):
        session.run(self._lr_update, feed_dict={self._new_lr: lr_value})
    
    @property
    def input(self):
        return self._input
    
#     @property
#     def initial_state(self):
#         return self._initial_state
    
    @property
    def cost(self):
        return self._cost
    
#     @property
#     def final_state(self):
#         return self._final_state
    
    @property
    def lr(self):
        return self._lr
    
    @property
    def train_op(self):
        return self._train_op

class PTBInput(object):

    def __init__(self, config, data, name=None):
        self.batch_size = batch_size = config.batch_size
        self.num_steps = num_steps = config.num_steps
        self.epoch_size = ((len(data) // batch_size) - 1) // num_steps
        self.input_data, self.targets = reader.ptb_producer(data, batch_size, num_steps, name=name)


class SmallConfig(object):
    init_scale = 0.05
    learning_rate = 1
    max_grad_norm = 10
    num_layers = 2
    num_steps = 105
    hidden_size = 640
    max_epoch = 6
    max_max_epoch = 100
    keep_prob = 0
    lr_decay = 0.95
    batch_size = 20
    vocab_size = 10000



class MediumConfig(object):
    init_scale = 0.05
    learning_rate = 1.0
    max_grad_norm = 5
    num_layers = 2
    num_steps = 35
    hidden_size = 650
    max_epoch = 6
    max_max_epoch = 39
    keep_prob = 0.5
    lr_decay = 0.8
    batch_size = 20
    vocab_size = 10000


class LargeConfig(object):
    init_scale = 0.04
    learning_rate = 1.0
    max_grad_norm = 10
    num_layers = 2
    num_steps = 35
    hidden_size = 1500
    max_epoch = 14
    max_max_epoch = 55
    keep_prob = 0.35
    lr_decay = 1 / 1.15
    batch_size = 20
    vocab_size = 10000


class TestConfig(object):
    init_scale = 0.1
    learning_rate = 1.0
    max_grad_norm = 1
    num_layers = 1
    num_steps = 2
    hidden_size = 2
    max_epoch = 1
    max_max_epoch = 1
    keep_prob = 1.0
    lr_decay = 0.5
    batch_size = 20
    vocab_size = 10000


def run_epoch(session, model, eval_op=None, verbose=False):
    start_time = time.time()
    costs = 0.0
    iters = 0
    # init states to zero
    states = [qrnn_.initial_state.eval(session=session) for qrnn_ in model.qrnns]
    
    fetches = {
        "cost": model.cost,
        "final_state": model.final_state,
    }
    if eval_op is not None:
        fetches["eval_op"] = eval_op
    feed_dict = {}
    for step in range(model.input.epoch_size):
        for state, init_state in zip(states, model.initial_states):
            feed_dict.update({init_state: state})
        vals = session.run(fetches, feed_dict)
        cost = vals["cost"]
        states = vals["final_state"]
    
        costs += cost
        iters += model.input.num_steps
        
        if verbose and step % (model.input.epoch_size // 10) == 10:
            print("%.3f perplexity: %.3f speed: %.0f wps" % 
                (step * 1.0 / model.input.epoch_size, np.exp(costs / iters),
                 iters * model.input.batch_size / (time.time() - start_time)))
            sys.stdout.flush()

    return np.exp(costs / iters)


def get_config():
    if FLAGS.model == "small":
        return SmallConfig()
    elif FLAGS.model == "medium":
        return MediumConfig()
    elif FLAGS.model == "large":
        return LargeConfig()
    elif FLAGS.model == "test":
        return TestConfig()
    else:
        raise ValueError("Invalid model: %s", FLAGS.model)


def train():
    if not FLAGS.data_path:
        raise ValueError("Must set --data_path to PTB data directory")
    
    raw_data = reader.ptb_raw_data(FLAGS.data_path)
    train_data, valid_data, test_data, _ = raw_data
    
    config = get_config()
    eval_config = get_config()
    eval_config.batch_size = 1
    eval_config.num_steps = 1

    with tf.Graph().as_default():
        initializer = tf.random_uniform_initializer(-config.init_scale,
                                                    config.init_scale)
        
        with tf.name_scope("Train"):
            train_input = PTBInput(config=config, data=train_data, name="TrainInput")
            with tf.variable_scope("Model", reuse=None, initializer=initializer):
                m = PTBModel(is_training=True, config=config, input_=train_input)
                tf.summary.scalar("Training Loss", m.cost)
                tf.summary.scalar("Learning Rate", m.lr)
        
        with tf.name_scope("Valid"):
            valid_input = PTBInput(config=config, data=valid_data, name="ValidInput")
            with tf.variable_scope("Model", reuse=True, initializer=initializer):
                mvalid = PTBModel(is_training=False, config=config, input_=valid_input)
                tf.summary.scalar("Validation Loss", mvalid.cost)
        
        with tf.name_scope("Test"):
            test_input = PTBInput(config=eval_config, data=test_data, name="TestInput")
            with tf.variable_scope("Model", reuse=True, initializer=initializer):
                mtest = PTBModel(is_training=False, config=eval_config,
                                 input_=test_input)

        sv = tf.train.Supervisor(logdir=FLAGS.save_path)
        with sv.managed_session() as session:
            for i in range(config.max_max_epoch):
                lr_decay = config.lr_decay ** max(i + 1 - config.max_epoch, 0.0)
                m.assign_lr(session, config.learning_rate * lr_decay)
                
                print("Epoch: %d Learning rate: %.3f" % (i + 1, session.run(m.lr)))
                train_perplexity = run_epoch(session, m, eval_op=m.train_op,
                                             verbose=True)
                print("Epoch: %d Train Perplexity: %.3f" % (i + 1, train_perplexity))
                valid_perplexity = run_epoch(session, mvalid)
                print("Epoch: %d Valid Perplexity: %.3f" % (i + 1, valid_perplexity))
            
            test_perplexity = run_epoch(session, mtest)
            print("Test Perplexity: %.3f" % test_perplexity)
            
            if FLAGS.save_path:
                print("Saving model to %s." % FLAGS.save_path)
                sv.saver.save(session, FLAGS.save_path, global_step=sv.global_step)


if __name__ == "__main__":
    train()


