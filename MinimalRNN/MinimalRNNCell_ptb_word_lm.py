import time
import sys
import numpy as np
import tensorflow as tf
import reader
from tensorflow.python.ops import math_ops
from tensorflow.python.ops.rnn_cell_impl import RNNCell
from tensorflow.python.util import nest
from tensorflow.python.ops import variable_scope as vs
from tensorflow.python.ops import array_ops
from tensorflow.python.ops import init_ops
from tensorflow.python.ops import nn_ops
_BIAS_VARIABLE_NAME = "bias"
_WEIGHTS_VARIABLE_NAME = "kernel"

flags = tf.flags
logging = tf.logging

flags.DEFINE_string(
	"model", "small",
	"A type of model. Possible options are: small, medium, large.")
flags.DEFINE_string("data_path", "../data",
					"Where the training/test data is stored.")
flags.DEFINE_string("save_path", None,
					"Model output directory.")
flags.DEFINE_bool("use_fp16", False,
				  "Train using 16-bit floats instead of 32bit floats")

FLAGS = flags.FLAGS


def data_type():
	return tf.float16 if FLAGS.use_fp16 else tf.float32

# -*- coding: utf-8 -*-

# Copyright (C) 2017 by Akira TAMAMORI

# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program.  If not, see <http://www.gnu.org/licenses/>.

# Copyright 2015 The TensorFlow Authors. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

class MinimalRNNCell(RNNCell):
    """MinimalRNN.

       This implementation is based on:

       Minmin Chen,
       "MinimalRNN: Toward More Interpretable and
        Trainable Recurrent Neural Networks,"
       https://arxiv.org/abs/1711.06788
    """

    def __init__(self, num_units, activation=tf.tanh, reuse=None):
        self._num_units = num_units
        self._activation = activation

    @property
    def output_size(self):
        return self._num_units

    @property
    def state_size(self):
        return self._num_units

    def step(self, inputs, h_prev, scope=None):
        """Run one step of MinimalRNN."""

        with tf.variable_scope(scope or type(self).__name__):
            with tf.variable_scope("Inputs"):
                z = self._activation(
                    linear([inputs], self._num_units, use_bias=True))
            with tf.variable_scope("Gate"):
                u = tf.sigmoid(linear([z, h_prev],
                                      self._num_units, use_bias=True,
                                      bias_start=1.0))

            h = u * h_prev + (1 - u) * z

        return h, h


def linear(args, output_size, use_bias=False, bias_start=0.0, scope=None):
    """Linear map: sum_i(args[i] * W[i]), where W[i] is a variable.
    Args:
      args: a 2D Tensor or a list of 2D, batch x n, Tensors.
      output_size: int, second dimension of W[i].
      bias: boolean, whether to add a bias term or not.
      bias_start: starting value to initialize the bias; 0 by default.
      scope: VariableScope for the created subgraph; defaults to "Linear".

    Returns:
      A 2D Tensor with shape [batch x output_size] equal to
      sum_i(args[i] * W[i]), where W[i]s are newly created matrices.

    Raises:
      ValueError: if some of the arguments has unspecified or wrong shape.
    """
    if args is None or (isinstance(args, (list, tuple)) and not args):
        raise ValueError("`args` must be specified")
    if not isinstance(args, (list, tuple)):
        args = [args]

    # Calculate the total size of arguments on dimension 1.
    total_arg_size = 0
    shapes = [a.get_shape().as_list() for a in args]
    for shape in shapes:
        if len(shape) != 2:
            raise ValueError(
                "Linear is expecting 2D arguments: %s" % str(shapes))
        if not shape[1]:
            raise ValueError(
                "Linear expects shape[1] of arguments: %s" % str(shapes))
        else:
            total_arg_size += shape[1]

    # Now the computation.
    with tf.variable_scope(scope or "Linear"):
        matrix = tf.get_variable("Matrix", [total_arg_size, output_size])
        if len(args) == 1:
            res = tf.matmul(args[0], matrix)
        else:
            res = tf.matmul(tf.concat(args, 1), matrix)
        if use_bias is False:
            return res
        bias_term = tf.get_variable(
            "Bias", [output_size],
            initializer=tf.constant_initializer(bias_start))
    return res + bias_term

# class BasicRNNCell(RNNCell):

#   def __init__(self, num_units, activation=None, reuse=None):
#     super(BasicRNNCell, self).__init__(_reuse=reuse)
#     self._num_units = num_units
#     self._activation = activation or math_ops.tanh


#   @property
#   def state_size(self):
#     return self._num_units

#   @property
#   def output_size(self):
#     return self._num_units

#   def step(self, inputs, state):
#     """Most basic RNN: output = new_state = act(W * input + U * state + B)."""
#     output = self._activation(_linear([inputs, state], self._num_units, True))
#     return output, output

# def _linear(args,
#             output_size,
#             bias,
#             bias_initializer=None,
#             kernel_initializer=None):
#   if args is None or (nest.is_sequence(args) and not args):
#     raise ValueError("`args` must be specified")
#   if not nest.is_sequence(args):
#     args = [args]

#   # Calculate the total size of arguments on dimension 1.
#   total_arg_size = 0
#   shapes = [a.get_shape() for a in args]
#   for shape in shapes:
#     if shape.ndims != 2:
#       raise ValueError("linear is expecting 2D arguments: %s" % shapes)
#     if shape[1].value is None:
#       raise ValueError("linear expects shape[1] to be provided for shape %s, "
#                        "but saw %s" % (shape, shape[1]))
#     else:
#       total_arg_size += shape[1].value

#   dtype = [a.dtype for a in args][0]

#   # Now the computation.
#   scope = vs.get_variable_scope()
#   with vs.variable_scope(scope) as outer_scope:
#     weights = vs.get_variable(
#         _WEIGHTS_VARIABLE_NAME, [total_arg_size, output_size],
#         dtype=dtype,
#         initializer=tf.contrib.layers.xavier_initializer(seed=0))
#     if len(args) == 1:
#       res = math_ops.matmul(args[0], weights)
#     else:
#       res = math_ops.matmul(array_ops.concat(args, 1), weights)
#     if not bias:
#       return res
#     with vs.variable_scope(outer_scope) as inner_scope:
#       inner_scope.set_partitioner(None)
#       biases = vs.get_variable(
#           _BIAS_VARIABLE_NAME, [output_size],
#           dtype=dtype,
#           initializer=tf.contrib.layers.xavier_initializer(seed=0))
#     return nn_ops.bias_add(res, biases)

class PTBInput(object):

	def __init__(self, config, data, name=None):
		self.batch_size = batch_size = config.batch_size
		self.num_steps = num_steps = config.num_steps
		self.epoch_size = ((len(data) // batch_size) - 1) // num_steps
		self.input_data, self.targets = reader.ptb_producer(data, batch_size, num_steps, name=name)


class PTBModel(object):

	def __init__(self, is_training, config, input_):
		self._input = input_
	
		batch_size = input_.batch_size
		num_steps = input_.num_steps
		size = config.hidden_size
		vocab_size = config.vocab_size
		

		# Slightly better results can be obtained with forget gate biases
		# initialized to 1 but the hyperparameters of the model would need to be
		# different than reported in the paper.
		rnn_cell = MinimalRNNCell(size)
		#if is_training and config.keep_prob < 1:
		#	lstm_cell = tf.nn.rnn_cell.DropoutWrapper(lstm_cell, output_keep_prob=config.keep_prob)
		#cell = tf.nn.rnn_cell.MultiRNNCell([lstm_cell] * config.num_layers, state_is_tuple=True)
	
		self._initial_state = rnn_cell.zero_state(batch_size, data_type())
	
		with tf.device("/cpu:0"):
			embedding = tf.get_variable("embedding", [vocab_size, size], dtype=data_type(), initializer=tf.contrib.layers.xavier_initializer(seed=0))
			inputs = tf.nn.embedding_lookup(embedding, input_.input_data)
	
		if is_training and config.keep_prob < 1:
			inputs = tf.nn.dropout(inputs, config.keep_prob, seed=0)
	
		
		outputs = []
		state = self._initial_state
		with tf.variable_scope("RNN"):
			for time_step in range(num_steps):
				if time_step > 0: 
					tf.get_variable_scope().reuse_variables()
				(cell_output, state) = rnn_cell.step(inputs[:, time_step, :], state)
				outputs.append(cell_output)
	
		output = tf.reshape(tf.concat(outputs, 1), [-1, size])
		softmax_w = tf.get_variable(
			"softmax_w", [size, vocab_size], dtype=data_type(), initializer=tf.contrib.layers.xavier_initializer(seed=0))
		softmax_b = tf.get_variable("softmax_b", [vocab_size], dtype=data_type(), initializer=tf.contrib.layers.xavier_initializer(seed=0))
		logits = tf.matmul(output, softmax_w) + softmax_b
		loss = tf.contrib.legacy_seq2seq.sequence_loss_by_example(
				[logits],
				[tf.reshape(input_.targets, [-1])],
				[tf.ones([batch_size * num_steps], dtype=data_type())])
		self._cost = cost = tf.reduce_sum(loss) / batch_size
		self._final_state = state
	
		if not is_training:
			return
	
		self._lr = tf.Variable(0.0, trainable=False)
		tvars = tf.trainable_variables()
		grads, _ = tf.clip_by_global_norm(tf.gradients(cost, tvars),
										  config.max_grad_norm)
		optimizer = tf.train.GradientDescentOptimizer(self._lr)
		self._train_op = optimizer.apply_gradients(
			zip(grads, tvars),
			global_step=tf.contrib.framework.get_or_create_global_step())
	
		self._new_lr = tf.placeholder(tf.float32, shape=[], name="new_learning_rate")
		self._lr_update = tf.assign(self._lr, self._new_lr)

	def assign_lr(self, session, lr_value):
		session.run(self._lr_update, feed_dict={self._new_lr: lr_value})
	
	@property
	def input(self):
		return self._input
	
	@property
	def initial_state(self):
		return self._initial_state
	
	@property
	def cost(self):
		return self._cost
	
	@property
	def final_state(self):
		return self._final_state
	
	@property
	def lr(self):
		return self._lr
	
	@property
	def train_op(self):
		return self._train_op


class SmallConfig(object):

	init_scale = 0.05
	learning_rate = 0.1
	max_grad_norm = 5
	num_layers = 1
	num_steps = 35
	hidden_size = 650
	max_epoch = 6
	max_max_epoch = 39
	keep_prob = 0.5
	lr_decay = 0.8
	batch_size = 20
	vocab_size = 10000


class MediumConfig(object):
	init_scale = 0.05
	learning_rate = 1.0
	max_grad_norm = 5
	num_layers = 2
	num_steps = 35
	hidden_size = 650
	max_epoch = 6
	max_max_epoch = 39
	keep_prob = 0.5
	lr_decay = 0.8
	batch_size = 20
	vocab_size = 10000


class LargeConfig(object):
	init_scale = 0.04
	learning_rate = 1.0
	max_grad_norm = 10
	num_layers = 2
	num_steps = 35
	hidden_size = 1500
	max_epoch = 14
	max_max_epoch = 55
	keep_prob = 0.35
	lr_decay = 1 / 1.15
	batch_size = 20
	vocab_size = 10000


class TestConfig(object):
	init_scale = 0.1
	learning_rate = 1.0
	max_grad_norm = 1
	num_layers = 1
	num_steps = 2
	hidden_size = 2
	max_epoch = 1
	max_max_epoch = 1
	keep_prob = 1.0
	lr_decay = 0.5
	batch_size = 20
	vocab_size = 10000


def run_epoch(session, model, eval_op=None, verbose=False):
	start_time = time.time()
	costs = 0.0
	iters = 0
	state = session.run(model.initial_state)
	
	fetches = {
		"cost": model.cost,
		"final_state": model.final_state,
	}
	if eval_op is not None:
		fetches["eval_op"] = eval_op

	for step in range(model.input.epoch_size):
		feed_dict = {model.initial_state:state}
		# for i, (c, h) in enumerate(model.initial_state):
		# 	feed_dict[c] = state[i].c
		# 	feed_dict[h] = state[i].h


		vals = session.run(fetches, feed_dict)
		cost = vals["cost"]
		state = vals["final_state"]
	
		costs += cost
		iters += model.input.num_steps
		
		if verbose and step % (model.input.epoch_size // 10) == 10:
			print("%.3f perplexity: %.3f speed: %.0f wps" % 
				(step * 1.0 / model.input.epoch_size, np.exp(costs / iters),
				 iters * model.input.batch_size / (time.time() - start_time)))
			sys.stdout.flush()

	return np.exp(costs / iters)


def get_config():
	if FLAGS.model == "small":
		return SmallConfig()
	elif FLAGS.model == "medium":
		return MediumConfig()
	elif FLAGS.model == "large":
		return LargeConfig()
	elif FLAGS.model == "test":
		return TestConfig()
	else:
		raise ValueError("Invalid model: %s", FLAGS.model)


def train():
	if not FLAGS.data_path:
		raise ValueError("Must set --data_path to PTB data directory")
	
	raw_data = reader.ptb_raw_data(FLAGS.data_path)
	train_data, valid_data, test_data, _ = raw_data
	
	config = get_config()
	eval_config = get_config()
	#eval_config.batch_size = 1
	eval_config.num_steps = 1

	with tf.Graph().as_default():

		tf.set_random_seed(0)

		initializer = tf.random_uniform_initializer(-config.init_scale,
													config.init_scale, seed=0)
		
		with tf.name_scope("Train"):
			train_input = PTBInput(config=config, data=train_data, name="TrainInput")
			with tf.variable_scope("Model", reuse=None, initializer=initializer):
				m = PTBModel(is_training=True, config=config, input_=train_input)
				tf.summary.scalar("Training Loss", m.cost)
				tf.summary.scalar("Learning Rate", m.lr)
		
		with tf.name_scope("Valid"):
			valid_input = PTBInput(config=config, data=valid_data, name="ValidInput")
			with tf.variable_scope("Model", reuse=True, initializer=initializer):
				mvalid = PTBModel(is_training=False, config=config, input_=valid_input)
				tf.summary.scalar("Validation Loss", mvalid.cost)
		
		with tf.name_scope("Test"):
			test_input = PTBInput(config=eval_config, data=test_data, name="TestInput")
			with tf.variable_scope("Model", reuse=True, initializer=initializer):
				mtest = PTBModel(is_training=False, config=eval_config,
								 input_=test_input)

		sv = tf.train.Supervisor(logdir=FLAGS.save_path)
		with sv.managed_session() as session:
			for i in range(config.max_max_epoch):
				lr_decay = config.lr_decay ** max(i + 1 - config.max_epoch, 0.0)
				m.assign_lr(session, config.learning_rate * lr_decay)
				
				print("Epoch: %d Learning rate: %.3f" % (i + 1, session.run(m.lr)))
				train_perplexity = run_epoch(session, m, eval_op=m.train_op,
											 verbose=True)
				print("Epoch: %d Train Perplexity: %.3f" % (i + 1, train_perplexity))
				valid_perplexity = run_epoch(session, mvalid)
				print("Epoch: %d Valid Perplexity: %.3f" % (i + 1, valid_perplexity))
			
			test_perplexity = run_epoch(session, mtest)
			print("Test Perplexity: %.3f" % test_perplexity)
			
			if FLAGS.save_path:
				print("Saving model to %s." % FLAGS.save_path)
				sv.saver.save(session, FLAGS.save_path, global_step=sv.global_step)


if __name__ == "__main__":
	train()


