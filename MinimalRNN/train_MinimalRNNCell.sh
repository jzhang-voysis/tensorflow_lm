#!/bin/bash
#SBATCH --ntasks=1
#SBATCH --gres=gpu:1
#SBATCH --time=10-00:00
#SBATCH --mem-per-cpu=5000
#SBATCH --output=MinimalRNNCell_ptb_word_lm.out	#job name and job id
#SBATCH --job-name=MinimalRNNCell_ptb_word_lm



python /scratch/jzhang/tools/write_rc.py
source ~/.bashrc

python MinimalRNNCell_ptb_word_lm.py


