import tensorflow as tf

a = tf.random_uniform([1,20], minval=-2, maxval=2, dtype=tf.int32, seed=None, name=None)
t = tf.clip_by_value(a, 0, 255)
sess = tf.Session()
sess.run(tf.global_variables_initializer())

print (sess.run([a, t]))