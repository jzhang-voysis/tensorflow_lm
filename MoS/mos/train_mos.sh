#!/bin/bash
#SBATCH --ntasks=1
#SBATCH --gres=gpu:1
#SBATCH --time=10-00:00
#SBATCH --mem-per-cpu=10000
#SBATCH --output=ptb_mos.out    #job name and job id
#SBATCH --job-name=ptb_mos



python /scratch/jzhang/tools/write_rc.py
source ~/.bashrc

#python main.py

python main.py --data ../../data/ \
              --dropouti 0.4 \
              --dropoutl 0.29 \
              --dropouth 0.225 \
              --seed 28 \
              --batch_size 12 \
              --lr 20.0 \
              --epoch 1000 \
              --nhid 960 \
              --nhidlast 620 \
              --emsize 280 \
              --n_experts 15 \
              --save PTB \
              --single_gpu
